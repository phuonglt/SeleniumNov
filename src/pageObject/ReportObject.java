package pageObject;

import org.openqa.selenium.By;

public class ReportObject {
	// add issue
	public static By snpCategory = By.xpath("html/body/div[3]/form/table/tbody/tr[2]/td[2]/select");
	public static By txtSummary = By.name("summary");
	public static By txtDescription = By.name("description");
	public static By btnAdd = By.xpath("html/body/div[3]/form/table/tbody/tr[15]/td[2]/input");
}
