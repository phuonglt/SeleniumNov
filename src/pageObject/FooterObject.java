package pageObject;

import org.openqa.selenium.By;

public class FooterObject {
	public static By bgNew = By.xpath("//td[contains(.,'new')]");
	public static By bgFeedBack = By.xpath("//td[contains(.,'feedback')]");
	public static By bgAcknowledged = By.xpath("//td[contains(.,'acknowledged')]");
	public static By bgConfirmed = By.xpath("//td[contains(.,'confirmed')]");
	public static By bgAssigned = By.xpath("//td[contains(@bgcolor,'#c2dfff')]");
	public static By bgResolved = By.xpath("//td[contains(.,'resolved')]");
	public static By bgClosed = By.xpath("//td[contains(.,'closed')]");
}
