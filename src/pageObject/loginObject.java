package pageObject;

import org.openqa.selenium.By;

public class loginObject {
	// user name text box
	public static By txtUsername = By.name("username");

	// pass word
	public static By txtPassword = By.name("password");

	// log in button
	public static By btnSubmit = By.xpath("html/body/div[3]/form/table/tbody/tr[6]/td/input");
	
	// verify title
	public static By txtTitle = By.xpath("html/body/div[1]/a/img");
	
	// verify image
	public static By image = By.xpath("html/body/div[1]/a/img");
	

}
