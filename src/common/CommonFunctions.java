package common;

import java.io.IOException;

import org.openqa.selenium.firefox.FirefoxDriver;

import pageObject.loginObject;

public class CommonFunctions {
	public static void commonOpenFireFox() {
		System.setProperty("webdriver.gecko.driver", utils.Geckodriver);
		utils.driver = new FirefoxDriver();
	}

	public static void commonLogIn() throws InterruptedException, IOException {
		utils.driver.get(utils.URL);
		utils.driver.findElement(loginObject.txtUsername).sendKeys(utils.Username);
		utils.driver.findElement(loginObject.txtPassword).sendKeys(utils.Password);
		utils.driver.findElement(loginObject.btnSubmit).click();
		Thread.sleep(3000);
	}

	public static void commonLogout() {
		utils.driver.quit();

	}

}
