import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.TakesScreenshot;

public class JUnit {
	private WebDriver driver; // constant driver

	@Before
	public void before() {
		System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
		driver = new FirefoxDriver();
		// driver.quit();
	}

	@Test
	public void login() throws InterruptedException, IOException {
		driver.get("https://tr-fukuri.jp/signin");
		driver.findElement(By.xpath(".//*[@id='ipt_id_account']")).sendKeys("phuong-lt");
		driver.findElement(By.xpath(".//*[@id='ipt_id_password']")).sendKeys("123456-p");
		driver.findElement(By.xpath(".//*[@id='btn_login']")).click();
		Thread.sleep(3000);
		// Take screenshot
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src, new File("images/ScreenShot.png"));
		// wait 8s
		Thread.sleep(8000);
		// Text verify
		String VerifyText = driver.findElement(By.id("spn_id_user_name")).getText();
		Assert.assertEquals("Phuong LT", VerifyText);
		// driver.findElement(By.id("ipt_id_account")).click();

		// verify lo go size
		int width = driver.findElement(By.xpath(".//*[@id='global-nav']/a/img")).getSize().getWidth();
		System.out.println(width);
		Assert.assertEquals(width, 30);
	}

	@After
	public void after() {
		// sign out
		driver.findElement(By.xpath(".//*[@id='dropdown-menu']")).click();
		driver.findElement(By.xpath(".//*[@id='navbarSupportedContent']/div/ul/li[2]/div/a[4]")).click();
		driver.quit();
		// Text verify
		String VerifyText = driver.findElement(By.id("btn_login")).getText();
		Assert.assertEquals("Sign in", VerifyText);

	}

}
