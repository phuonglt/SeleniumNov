package test;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import common.ExcelCommon_POI;
import common.CommonFunctions;
import common.utils;
import pageObject.loginObject;

public class ExcelLogin {

	// KB excel
	public String ExcelName = "TestCaseLogin.xlsx";
	public String SheetName_Login = "Login";

	@Before
	public void before() {
		CommonFunctions.commonOpenFireFox();
		utils.driver.get(utils.URL);
	}

	@Test
	public void ExcelLoginFilter() throws Exception {

		// Read data from excel
		XSSFSheet ExcelDataSheet = ExcelCommon_POI.setExcelFile(ExcelName, SheetName_Login);
		String Username = ExcelCommon_POI.getCellData(0, 1, ExcelDataSheet);
		String Password = ExcelCommon_POI.getCellData(0, 3, ExcelDataSheet);
		Thread.sleep(4000);

		utils.driver.findElement(loginObject.txtUsername).sendKeys(Username);
		utils.driver.findElement(loginObject.txtPassword).sendKeys(Password);
		utils.driver.findElement(loginObject.btnSubmit).click();
		Thread.sleep(8000);

		// Write to excel
		if (utils.driver.getPageSource().contains("Logged")) {
			ExcelCommon_POI.setCellData(0, 4, ExcelName, SheetName_Login, "Passed");
		} else
			ExcelCommon_POI.setCellData(0, 4, ExcelName, SheetName_Login, "Failed");

	}

	@After
	public void after() {
		CommonFunctions.commonLogout();
	}

}
