package test;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.firefox.FirefoxDriver;

import common.utils;
import pageObject.loginObject;

public class logInSuccessful {
	@Before

	public void before() {

		System.setProperty("webdriver.gecko.driver", utils.Geckodriver);
		utils.driver = new FirefoxDriver();

	}

	@Test

	public void check() throws InterruptedException, IOException {

		utils.driver.get(utils.URL);
		utils.driver.findElement(loginObject.txtUsername).sendKeys(utils.Username);
		utils.driver.findElement(loginObject.txtPassword).sendKeys(utils.Password);
		utils.driver.findElement(loginObject.btnSubmit).click();
		Thread.sleep(8000);

		// take screen shoot
		File src = ((TakesScreenshot) utils.driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src, new File("images/ScreenShot.png"));
		Thread.sleep(1000);

		// verify text
		String verify = utils.driver.findElement(loginObject.txtTitle).getText();
		Assert.assertEquals("mantis", verify);

		// verify image
		int width = utils.driver.findElement(loginObject.image).getSize().getWidth();
		System.out.println(width);
		Assert.assertEquals(width, 50);
		Thread.sleep(2000);

	}

	@After
	public void after() throws Exception {
		utils.driver.quit();
	}

}
