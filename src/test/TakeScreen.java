package test;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import common.CommonFunctions;
import common.utils;

public class TakeScreen {
	@Before
	public void before() {
		CommonFunctions.commonOpenFireFox();
	}

	@Test
	public void check() throws InterruptedException, IOException {
		CommonFunctions.commonLogIn();

		// Take many screenshot

		Date d = new Date();
		// System.out.println(d.toString());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
		File scrFile = ((TakesScreenshot) utils.driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("images/ScreenShot" + sdf.format(d) + ".png"));
	}

	@After
	public void after() {
		CommonFunctions.commonLogout();

	}

}
