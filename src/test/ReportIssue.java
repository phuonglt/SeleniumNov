package test;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import common.CommonFunctions;
import common.utils;
import pageObject.ReportObject;

public class ReportIssue {
	@Before

	public void before() {
		CommonFunctions.commonOpenFireFox();
	}

	@Test

	public void check() throws InterruptedException, IOException {
		CommonFunctions.commonLogIn();
		
		// add issue
		utils.driver.findElement(By.linkText("Report Issue")).click();
		Thread.sleep(2000);
		utils.driver.findElement(ReportObject.snpCategory).sendKeys("[All Projects] General");
		utils.driver.findElement(ReportObject.txtSummary).sendKeys("LePhuong Summary");
		utils.driver.findElement(ReportObject.txtDescription).sendKeys("LePhuong discription");
		utils.driver.findElement(ReportObject.btnAdd).click();
		Thread.sleep(3000);

	}

	@After
	public void after() throws Exception {
		utils.driver.quit();
	}

}
