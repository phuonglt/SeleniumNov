package test;

import java.io.IOException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import common.CommonFunctions;
import common.utils;
import pageObject.FooterObject;

public class VerifyFooterColor {
	@Before
	public void before() {
		CommonFunctions.commonOpenFireFox();
	}

	@Test
	public void check() throws InterruptedException, IOException {
		CommonFunctions.commonLogIn();
		Thread.sleep(2000);

		// Verify color
		String colorNew = utils.driver.findElement(FooterObject.bgNew).getAttribute("bgcolor");
		System.out.println(colorNew);
		Thread.sleep(2000);
		Assert.assertEquals("#fcbdbd", colorNew);

		String colorFeedBack = utils.driver.findElement(FooterObject.bgFeedBack).getAttribute("bgcolor");
		System.out.println(colorFeedBack);
		Thread.sleep(2000);
		Assert.assertEquals("#e3b7eb", colorFeedBack);

		String colorAcknowledged = utils.driver.findElement(FooterObject.bgAcknowledged).getAttribute("bgcolor");
		System.out.println(colorAcknowledged);
		Thread.sleep(2000);
		Assert.assertEquals("#ffcd85", colorAcknowledged);

		String colorConfirmed = utils.driver.findElement(FooterObject.bgConfirmed).getAttribute("bgcolor");
		System.out.println(colorConfirmed);
		Thread.sleep(2000);
		Assert.assertEquals("#fff494", colorConfirmed);

		String colorAssigned = utils.driver.findElement(FooterObject.bgAssigned).getAttribute("bgcolor");
		System.out.println(colorAssigned);
		Thread.sleep(2000);
		Assert.assertEquals("#c2dfff", colorAssigned);

		String colorResolved = utils.driver.findElement(FooterObject.bgResolved).getAttribute("bgcolor");
		System.out.println(colorResolved);
		Thread.sleep(2000);
		Assert.assertEquals("#d2f5b0", colorResolved);

		String colorClosed = utils.driver.findElement(FooterObject.bgClosed).getAttribute("bgcolor");
		System.out.println(colorClosed);
		Thread.sleep(2000);
		Assert.assertEquals("#c9ccc4", colorClosed);

	}

	@After
	public void after() {
		CommonFunctions.commonLogout();
	}

}
